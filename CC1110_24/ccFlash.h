//
#include <stdint.h>
#include "cc1111.h"
// Flash write timer value:
// FWT = 21000 * FCLK / (16 * 10^9)
// For FCLK = 24MHz, FWT = 0x1F
#define FLASH_FWT (0x1F>>1)
// Address of flash controller data register
#define FLASH_FWDATA_ADDR 0xDFAF
//void eraseFlash(uint16_t flash_addr);
void eraseFlash(void);
//void writeFlash(uint16_t buf, uint16_t cnt, uint16_t flash_addr);
void writeFlash(void);
//void dmaFlashWrite(uint16_t buf, uint16_t cnt, uint16_t flash_addr);
void dmaFlashWrite(void);
//
void eraseFlash(void)
{
    // Waiting for the flash controller to be ready
    while (FCTL & FCTL_BUSY);
    // Configure flash controller for a flash page erase
    // FADDRH[5:1] contains the page to erase
    // FADDRH[1]:FADDRL[7:0] contains the address within the page
    // (16-bit word addressed)
    FWT = FLASH_FWT;
    FADDRH = flashAddr>> 9;
    FADDRL = 0;
    // Erase the page that will be written to
    FCTL |=  FCTL_ERASE;
    nop; // Required, see datasheet
    // Wait for the erase operation to complete
    while (FCTL & FCTL_BUSY);
    //FCTL &=  ~FCTL_ERASE;
}
//
void writeFlash(void)
{
    uint8_t j;
//          Really FlashAdr = (FlashAdr>>1)<<1 !!!!!
//    if((flashAddr < USER_CODE_BASE) && (Flags&FlU)) return;
    if(flashAddr < USER_CODE_BASE) return;
    if(flashCnt==0xFF)
    {
        eraseFlash();
        flashCnt=0;
        return;
    }
    if((flashAddr+flashCnt)>0x8000) flashCnt=0x8000-flashAddr;
    if(flashAddr&1)
    {
        for(j=flashCnt; j>0; j--) flashBuf[j]=flashBuf[j-1];
        flashBuf[0]=0xFF;
        --flashAddr;
        ++flashCnt;
    }
    if(flashCnt&1) flashBuf[flashCnt++]=0xFF;
    dmaFlashWrite();
    flashCnt=0;
}
//
//ccFlash.h|43|warning 127: non-pointer type cast to generic pointer|
void dmaFlashWrite(void)
{
//          Really FlashAdr = (FlashAdr>>1)<<1 !!!!!
//          Configure DMA channel 0. Settings:
//    SRCADDR: address of the data to be written to flash (increasing).
//    DESTADDR: the flash controller data register (fixed), so that the
//    flash controller will write this data to flash.
//    VLEN: use LEN for transfer count.
//    LEN: equal to the number of bytes to be transferred.
//    WORDSIZE: each transfer should transfer one byte.
//    TMODE: should be set to single mode (see datasheet, DMA Flash Write).
//    Each flash write complete will re-trigger the DMA channel.
//    TRIG: let the DMA channel be triggered by flash data write complete
//    (trigger number 18). That is, the flash controller will trigger the
//    DMA channel when the Flash Write Data register, FWDATA, is ready to
//    receive new data.
//    SRCINC: increment by one byte.
//    DESTINC: fixed (always write to FWDATA).
//    IRQMASK: disable interrupts from this channel.
//    M8: 0, irrelevant since we use LEN for transfer count.
//    PRIORITY: low.

    if(flashCnt < 2) return;
    FWT = FLASH_FWT;
// Waiting for the flash controller to be ready
    while (tBit(FCTL,7)); // Busy
    dmaCfg.src_high = ((uint16_t)&flashBuf)>>8;
    if(dmaCfg.src_high==0) dmaCfg.src_high=0xFF;
    dmaCfg.src_low  = ((uint16_t)&flashBuf)&0x00FF;
    dmaCfg.dst_high = FLASH_FWDATA_ADDR >> 8;    // 0xDF; FWDATA // DESTADDRH
    dmaCfg.dst_low  = FLASH_FWDATA_ADDR&0x00FF;  // 0xAF;        // DESTADDRL
    dmaCfg.len_high=0;  //flashCnt>>8;
    dmaCfg.len_low=flashCnt&0x00FF;
    if(dmaCfg.len_low&1) dmaCfg.len_low&=~1;
    dmaCfg.cfg0=DMA_CFG0_WORDSIZE_8|DMA_CFG0_TMODE_SINGLE|DMA_CFG0_TRIGGER_FLASH;
    dmaCfg.cfg1=DMA_CFG1_SRCINC_1|DMA_CFG1_DESTINC_0;
    DMA0CFGH  = ((uint16_t)&dmaCfg)>>8;
    if(DMA0CFGH==0) DMA0CFGH=0xFF;
    DMA0CFGL  = ((uint16_t)&dmaCfg)&0x00FF;
    FADDRH = flashAddr>>9;
    FADDRL = (flashAddr>>1)&0x00FF;
    DMAARM = 0;
    DMAIRQ = 0;
    sBit(DMAARM, 0);
//    StartFlashWrite();
    if(((uint16_t)&dmaFlashWrite)&1)
    {
        nop;
        nop;
    }
    else
    {
        nop;
    }
    sBit(FCTL, 1);
    nop;
    while (!tBit(DMAIRQ,0));
    while (tBit(FCTL,6) || tBit(FCTL,7));
    cBit(DMAIRQ, 0);
    cBit(DMAARM, 0);
}
//
