#define BaseFreq    868300000   // 868,3 MHz
#define SyncWord    0xD391
// Command Strobes
/*  Enable and calibrate frequency synthesizer
 (if  MCSM0.FS_AUTOCAL=01). If in RX (with CCA):
 Go to a wait state where only the synthesizer is
 running (for quick RX / TX turnaround). */
#define SFSTXON()     RFST = 0x00
/* Calibrate frequency synthesizer and turn it off.
 SCALcan be strobed from IDLE mode without setting
 manual calibration mode (MCSM0.FS_AUTOCAL=00) */
#define SCAL()        RFST = 0x01
/* Enable RX. Perform calibration first if coming
 from IDLE and  MCSM0.FS_AUTOCAL=01. */
#define SRX()         RFST = 0x02
/* In IDLE state: Enable TX. Perform calibration
 first if MCSM0.FS_AUTOCAL=01.
 If in RX state and CCA is enabled:
 Only go to TX if channel is clear. */
#define STX()         RFST = 0x03
/* Enter IDLE state (frequency synthesizer turned off). */
#define SIDLE()       RFST = 0x04
/* No operation. */
#define SNOP()        RFST = 0xFF
//
void initRadio(void)
{
    RFIF = 0; // Clear RF interrupt flag
    RFIM = 0; // Clear RF interrupt enable mask
    SIDLE();
    /* Deviation = 5.554199 */
    /* Base frequency = 867.639709 */
    /* Carrier frequency = 867.639709 */
    /* Channel number = 0 */
    /* Modulated = true */
    /* Modulation format = GFSK */
    /* Manchester enable = false */
    /* Sync word qualifier mode = 16/16 sync word bits detected */
    /* Preamble count = 4 */
    /* Channel spacing = 59.906006 */
    /* Data rate = 2.39897 */
    /* RX filter BW = 58.035714 */
    /* Length config = Variable packet length mode. Packet length configured by the first byte after sync word */
    /* CRC enable = true */
    /* Packet length = 255 */
    /* Address config = No address check */
    /* PA ramping = false */
    /* TX power = 10 */
    /* RF settings SoC WM-Bus -F2A */
    SYNC1      = 0xF6; // sync word, high byte
    SYNC0      = 0x8D; // sync word, low byte
//    SYNC1 = SyncWord>>8;
//    SYNC0 = SyncWord&0x00FF;
    PKTLEN      = 0xFF;   // packet length
    PKTCTRL1    = 0x00; // no status
    PKTCTRL0    = 0x00; // no crc fixed length
    ADDR        = 0x00; // device address
    CHANNR      = 0x00; // channel number
    FSCTRL1     = 0x06; // IF freq = 152 kHz
    FSCTRL0     = 0x00; // frequency synthesizer control
    FREQ2       = 0x24; // frequency control word, high byte
    FREQ1       = 0x26; // frequency control word, middle byte
    FREQ0       = 0xD3; // frequency control word, low byte
    MDMCFG4     = 0xF6; // RX filter BW = 58.035714
    MDMCFG3     = 0xA3; // Data rate = 2.39897
    MDMCFG2     = 0x12; // 16/16 above thr GFSK
    MDMCFG1     = 0x51; // 12 byte preamble
    MDMCFG0     = 0x47; // Channel spacing = 59.906006
    DEVIATN     = 0x17; // Deviation = 5.554199
    MCSM2       = 0x07; // Until end of packet
    MCSM1       = 0x00; // CCA always + IDLE
    MCSM0       = 0x18; // When going from IDLE to RX or TX
    FOCCFG      = 0x17; // frequency offset compensation configuration
    BSCFG       = 0x6C; // bit synchronization configuration
    AGCCTRL2    = 0x43; // agc control
    AGCCTRL1    = 0x40; // agc control
    AGCCTRL0    = 0x91; // agc control
    FREND1      = 0x56; // front end rx configuration
    FREND0      = 0x10; // front end tx configuration
    FSCAL3      = 0xE9; // frequency synthesizer calibration
    FSCAL2      = 0x2A; // frequency synthesizer calibration
    FSCAL1      = 0x00; // frequency synthesizer calibration
    FSCAL0      = 0x1F; // frequency synthesizer calibration
    TEST2       = 0x81; // various test settings
    TEST1       = 0x35; // various test settings
    TEST0       = 0x09; // various test settings
    PA_TABLE7   = 0x00; // pa power setting 7
    PA_TABLE6   = 0x00; // pa power setting 6
    PA_TABLE5   = 0x00; // pa power setting 5
    PA_TABLE4   = 0x00; // pa power setting 4
    PA_TABLE3   = 0x00; // pa power setting 3
    PA_TABLE2   = 0x00; // pa power setting 2
    PA_TABLE1   = 0x00; // pa power setting 1
//    PA_TABLE0   = 0x03; // -30 dBm
    PA_TABLE0   = 0x50;         // Output power: +0 dBm
//    PA_TABLE0   = 0x84;         // Output power: +5 dBm
//    PA_TABLE0   = 0xC2;         // Output power: +10 dBm
    IOCFG2      = 0x00; // radio test signal configuration (p1_7)
    IOCFG1      = 0x00; // radio test signal configuration (p1_6)
//    IOCFG0      = 0x06; // radio test signal configuration (p1_5)
//   PARTNUM    = 0x01; // chip id[15:8]
//  VERSION    = 0x03; // chip id[7:0]
//    FREQEST    = 0x00; // frequency offset estimate from demodulator
//  LQI        = 0x00; // demodulator estimate for link quality
//  RSSI       = 0x80; // received signal strength indication
//  MARCSTATE  = 0x01; // main radio control state machine state
//  PKTSTATUS  = 0x00; // packet status
//   VCO_VC_DAC = 0x94; // current setting from pll calibration module
    SIDLE();
    SCAL();
// Wait for calibration to complete
    while (MARCSTATE != 0x01);
    cbit(IEN0, 0);
// RF TX/RX done interrupt disable
    cbit(IEN2, 0);
    RFIM = 0;
    RFIF = 0;
    S1CON = 0; // Clear interrupt flag for SFD in RFIF
    cbit(TCON, 1);
//
}
//
unsigned char rfRcv(unsigned char *buf)
{
// receive maxPacket byte packet from RF
    unsigned char length = 0;
    PKTLEN      = 0xFF;
    SRX();
// SYNC detect
    while(!tbit(RFIF, 0));
    while(length < maxPacket) // rec Data maxPacket byte
        if(tbit(TCON, 1))
        {
            cbit(TCON, 1);
            *buf++ = RFD;
            ++length;
        };
// wait idle
    SIDLE();
    while(MARCSTATE != 0x01);
    cbit(TCON, 1);
    RFIF=0;
    S1CON=0;
    return length;
}
/* send packet to RF */
void rfSend(unsigned char *buf,unsigned char length)
{
    PKTLEN = length;   // packet length
    STX();
    while(length)
    {
        if(tbit(TCON, 1))
        {
            cbit(TCON, 1);
            RFD = *buf++;
            --length;
        };
    };
//    while(!tbit(TCON, 1));
//    cbit(TCON, 1);
//    SIDLE();
    while(MARCSTATE != 0x01); // idle
    cbit(TCON, 1);
    RFIF=0;
    S1CON=0;
}
