void writeFlash(void);
void RFTXRX_isr (void) __interrupt (RFTXRX_VECTOR)
{
    /*  RF TX done / RX ready                       */
}
//
void ADC_isr (void) __interrupt (ADC_VECTOR)
{
    /*  ADC End of Conversion                       */
}
//
void URX0_isr (void) __interrupt (URX0_VECTOR)
{
    /*  USART0 RX Complete                          */
    uint8_t c=U0DBUF;
    if(c==0x11 && Flags&FlU) // Ctrl-Q
    {
        WDCTL = (1 << 3); // enable 1 sec
        while (1);
    }
    else uartRX[rxIn++]=c;
}
//
void URX1_isr (void) __interrupt (URX1_VECTOR)
{
    /*  USART1 RX Complete                          */
}
//
void ENC_isr (void) __interrupt (ENC_VECTOR)
{
    /*  AES Encryption/Decryption Complete          */
}
//
void ST_isr (void) __interrupt (ST_VECTOR)
{
    /*  Sleep Timer Compare                         */
//       Note that the order in which the following flags are cleared is important.
//       For pulse or egde triggered interrupts one has to clear the CPU interrupt
//       flag prior to clearing the module interrupt flag.
// Clear [IRCON.STIF] (Sleep Timer CPU interrupt flag)
    cBit(IRCON,7);//STIF = 0;
// Clear [WORIRQ.EVENT0_FLAG] (Sleep Timer peripheral interrupt flag)
    WORIRQ &= ~(1<<0);  // WORIRQ_EVENT0_FLAG;
    ++TicksMain;
    RedLedOff;
    if((TicksMain%(BoardFreq/FBUTTON)) ==1)
    {
//        RedLedFlp;
        LedMask<<=1;
        if(LedMask==0) LedMask=1;
        if(LedMask&RedLedMask) RedLedOn;
//
        CheckButton();
    }
    if(TicksAlarm)
    {
        --TicksAlarm;
    }
    Flags|=FlT;
    if(txOut!=txIn) ENABLE_TX_UART();
//
    if(Flags&FlU)
    {
        if(UFI!=0) UserFunctionInterrupt();
        if(flashCnt) writeFlash();
    };
}
//
void P2INT_isr (void) __interrupt (P2INT_VECTOR)
{
//    uint8_t flags;
//    flags = P2IFG;
//    P2IFG=0;
//    IRCON2 &=~IRCON2_P2IF;  //    P2IF=0;
////    usb_isr();
//    /*  Port 2 Inputs                               */
//    ProcessP2_isr(flags);
}
//
void UTX0_isr (void) __interrupt (UTX0_VECTOR)
{
    /*  USART0 TX Complete                          */
// End of Transmit
    if(txOut==txIn)
    {
        DISABLE_TX_UART();//IEN2 &= ~IEN2_UTX0IE;
    }
    else
    {
        IRCON2&=~IRCON2_UTX0IF; //UTX0IF = 0;
        U0DBUF=uartTX[txOut++];
    }
}
//
void DMA_isr (void) __interrupt (DMA_VECTOR)
{
    /*  DMA Transfer Complete                       */
}
//
void T1_isr (void) __interrupt (T1_VECTOR)
{
    /*  Timer 1 (16-bit) Capture/Compare/Overflow   */
}
//
void T2_isr (void) __interrupt (T2_VECTOR)
{
    /*  Timer 2 (MAC Timer) Overflow                */
}
//
void T3_isr (void) __interrupt (T3_VECTOR)
{
    /*  Timer 3 (8-bit) Capture/Compare/Overflow    */
}
//
void T4_isr (void) __interrupt (T4_VECTOR)
{
    /*  Timer 4 (8-bit) Capture/Compare/Overflow    */
}
//
void P0INT_isr (void) __interrupt (P0INT_VECTOR)
{
/*  Port 0 Inputs                               */
//#define USB_RESUME_INT_ENABLE()    {PICTL |= 0x10;PICTL &=~0x01;IEN1|=IEN1_P0IE;}
//#define USB_RESUME_INT_DISABLE()   PICTL &=~0x10
//#define USB_RESUME_INT_CLEAR()    {P0IFG=0; P0IF=0;}
//    uint8_t flags;
//    flags = P0IFG;
//    P0IFG=0;
//    IRCON&=~IRCON_P0IF;//P0IF=0;
////
//    ProcessP0_isr(flags);
}
//
void UTX1_isr (void) __interrupt (UTX1_VECTOR)
{
    /*  USART1 TX Complete                          */

}
//
void P1INT_isr (void) __interrupt (P1INT_VECTOR)
{
    /*  Port 1 Inputs                               */
//    uint8_t flags;
//    flags = P1IFG;
//    P1IFG=0;
//    IRCON2&=~IRCON2_P1IF;
}
//
void  RF_isr (void) __interrupt ( RF_VECTOR)
{
    /*  RF General Interrupts                       */
}
//
void WDT_isr (void) __interrupt (WDT_VECTOR)
{
    /*  Watchdog Overflow in Timer Mode             */
}
//
