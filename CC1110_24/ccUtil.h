//
void eraseFlash(void);
//void spiTx(uint8_t *buf,uint8_t cnt)
//{
//    /***************************************************************************
//     * Transfer data
//     */
//    Gled_on();
//// Set SSN to active low
//    P0_4 = 0;
//    while(cnt--)
//    {
//        U0DBUF = *buf++;
//        while(!(U0CSR & UxCSR_TX_BYTE));
//        U0CSR &= ~UxCSR_TX_BYTE;
//    }
////    for (i = 0; i < BUFFER_SIZE; i++)
////    {
////        // Write uint8_t to USART0 buffer (transmit data)
////        U0DBUF = txBufferMaster[i];
////
////        // Check if uint8_t is transmitted
////        while(!(U0CSR & U0CSR_TX_uint8_t));
////
////        // Clear transmit uint8_t status
////        U0CSR &= ~U0CSR_TX_uint8_t;
////    }
//
//    // Set SSN to active high
//    P0_4 = 1;
//// When finished transmitting, set green LED
//    Gled_off();
//}
////
//void spiRx(uint8_t *buf,uint8_t cnt)
//{
//    Gled_on();
//// Set SSN to active low
//    P0_4 = 0;
//    while(cnt--)
//    {
//// Write dummy uint8_t to USART0 buffer (transmit data)
//        U0DBUF = 0x00;
//// Check if uint8_t is transmitted (and a uint8_t is recieved)
//        while(!(U0CSR & UxCSR_TX_BYTE))
//        {
//            nop();
//        }
//// Clear transmit uint8_t status
//        U0CSR &= ~UxCSR_TX_BYTE;
//        // Write received uint8_t to buffer
//        *buf++ = U0DBUF;
//    }
//// Set SSN to active high
//    P0_4 = 1;
//    Gled_off();
//}
//
void putchar(char c)
{
    uint8_t p;
    uartTX[txIn++]=c;
    ENABLE_TX_UART();
    do
    {
        p=(txIn-txOut);
    }
    while(p >= MAXUART);
//    U0DBUF=c;
//    while (!(U0CSR&UxCSR_TX_BYTE)) ; // TX_BYTE
//    U0CSR&=~UxCSR_TX_BYTE;
}
//
char getchar(void)
{
//    if(rxOut==rxIn) return 0;
//    else return uartRX[rxOut++];
    while(rxOut==rxIn)
    {
        if(!TicksAlarm && Flags&CmD) return 'U';
    };
    return uartRX[rxOut++];
}
//
//void printUart(void)
//{
//    printf_tiny("UART = %lu",CDC_LINE_CODING.dwDTERate);
//    printf_tiny(" %u",CDC_LINE_CODING.bStopBits);
//    printf_tiny(" %u",CDC_LINE_CODING.bParityType);
//    printf_tiny(" %u",CDC_LINE_CODING.bDataBits);
//    printf_tiny(" %u\n",CDC_LINE_CODING.bState);
//}
//void processUART(void)
//{
//    uint8_t c;
//    while((c=getchar())!=0x00)
//    {
////        Command=uartRxBuf[j];
//        switch (c)
//        {
//        case 'Q': /* Soft RESET  quit */
//            EA = 0;
//            WDCTL = (1 << 3); // enable 1 sec
//            while (1);
//            break;
//        case 'A':
////            printf_tiny("Hello world!\n");
////            printf_tiny("Adr = 0x%04X\n",UFI);
////            printUart();
//            break;
//        case 'U':
////            printf_tiny("Hello world!\n");
////            printf_tiny("Hello small world!\n");
////            printf_tiny("USB = %02X",USBINDEX);
////            printf_tiny(" %02X",USBINDEX);
////            printf_tiny(" %02X",CDC_LINE_CODING.bParityType);
////            printf_tiny(" %02X",CDC_LINE_CODING.bDataBits);
////            printf_tiny(" %02X\n",USBADDR);
//            break;
//        default:
//            break;
//        };
//    };
//    Flags&=~CmD;
//}
////
//void ProcessP0_isr(uint8_t flags)
//{
////    P0IFG=0;
////    IRCON.P0IFG=0;
////    flags=0;
//}
////
////
//void ProcessP2_isr(uint8_t flags)
//{
////    P0IFG=0;
////    IRCON.P0IFG=0;
////    flags=0;
//}
// FlashData eepromAdddr
void  FlashProgramming(void)
{
//    __xdata uint8_t *fA=USER_CODE_BASE;
//
//    uint8_t j,i;
//    for(i=0; i<8; i++)
//    {
//        for(j=0; j<16; j++)  printf_tiny("%02X",*(uint8_t *)fA++);
//        printf_tiny("\n");
//    }
//    __xdata uint8_t p[5]= {1,2,3,4,5};
//    prtFlash();
//    for(j=0; j<7;) flashBuf[j]=++j;
//    flashCnt=5;
//    flashAddr=eepromAddr+2;
//    writeFlash();
//    prtFlash();
//    eraseFlash();
//    prtFlash();
}
//struct button_t
//{
//    uint8_t state;
//    uint8_t cnt;
//} mButton= {0,128};
//
void CheckButton(void)
{
    ++butCnt;
    if(But)   // Not Pressed
    {
        if(butCnt > 254) 	butCnt=254;// Always 	released
        else if(butCnt  == (128+2*FBUTTON))
        {
//                printf_tiny("But=%02X\n",butState);
            ParseButCode(butState);
            butState = 1;
        }
        else if(butCnt < 128)   // Rising edge
        {
            if(butCnt > (FBUTTON+(FBUTTON>>1)))		// Press error
            {
                butState = 0;
            }
            else if(butCnt > (FBUTTON>>1)) // long
            {
                butState  |= 1;
            }
            else // short
            {
                butState  |= 0;
            };
            butCnt = 0x80;
        };
    }
    else        // Pressed
    {
//        U_ButRst;
        if(butCnt > 0x80)	// Now pressed
        {
            butState <<= 1;
            butCnt = 0;
        }
        else						// Always pressed
        {
            if( butCnt > 126) butCnt = 126;
            else if(butCnt == (FBUTTON<<2))
            {
#define SpecialCode 0x06
                ParseButCode(SpecialCode);
                butState = 0;
            };
        };
    };
}
//
//
void ParseButCode(uint8_t buttonCode)
{
//    uint8_t g;
//    printf_tiny("BUT=0x%02X\n",buttonCode);
    switch(buttonCode)
    {
//    case 0x06:   // Manual check Radio
////        lockJTAG();
//        CounterFlags.Install=1;
////        CounterState = csWait;
////        RadioDog=0;
//        break;
//    case 0x1C:   // Manual check Radio
////        lockJTAG();
//        CounterFlags.Probe=1;
////        CounterState = csWait;
////        RadioDog=0;
//        break;
////    case 0x1A:   // Power
//////        Command='C';
////        break;
////    case 0x0A:
//////        Command='G';
////        break;
//    case 0x09:
////        g=ReadSingleReg(CHANNR);
////        if(g==2) g=0;
////        else ++g;
////        halRfWriteReg(CHANNR, g);
//        break;
    case 0x28: // 0b0010 1000
//        printf_tiny("OK\nReset CHIP\n\n");
        IEN0&=~IEN0_EA;	//EA = 0;
        WDCTL = (1 << 3); // enable 1 sec
        while (1);
        break;
//    case 147:   // Russian U 0b1001 0011
////        G_LedOn;
////        DelayXT1(32768);
////        DelayXT1(32768);
////        DelayXT1(32768);
////        DelayXT1(32768);
//////        unlockJTAG();
////        R_LedMask = 147;
//        break;
//    case 139:   // Russian L 0b1000 1011
////        R_LedOn;
////        DelayXT1(52768);
////        DelayXT1(52768);
////        DelayXT1(52768);
////        DelayXT1(52768);
//////       lockJTAG();
////        R_LedMask = 139;
//        break;
    default:
        break;
    };
    butCode=butState;
}


const  uint8_t Help[]="\nCC1110 RFModem 868 MHz UART BOOTLOADER\n\
\nLinker Options --code-loc 0x2000 --xram-loc 0xf000\n\
CDC_LINE_CODING = {57600, 0, 0, 8}\n\
BoardFreq   32\n\
__xdata  __at (memAddr)     uint8_t uartRX[256];\n\
__xdata  __at (memAddr+256) uint8_t uartTX[256];\n\
__xdata  __at (memAddr+256+256) uint16_t UFI;\n\
__xdata  __at (memAddr+256+256) uint8_t param[64];\n\
userIntLow     param[0]\n\
userIntHigh    param[1]\n\
Flags          param[2] FlT (1<<7)\n\
RedLedMask     param[3]\n\
TicksMain      param[4]\n\
butCode        param[5]\n\
butCnt         param[6]\n\
TicksAlarm     param[7]\n\
rxIn           param[8]\n\
rxOut          param[9] (getchar())\n\
txIn           param[10](putchar())\n\
txOut          param[11]\n\
__xdata  __at (memAddr+256+256+12) uint8_t flashBuf[34];\n\
__xdata  __at (memAddr+256+256+12+34) uint8_t flashCnt;\n\
if(flashCnt==0xFF) {eraseFlash(); flashCnt=0;}\n\
__xdata  __at (memAddr+256+256+12+34+1) uint16_t flashAddr;\n\
__xdata  __at (memAddr+256+256+12+34+1+2) struct cc_dma_channel dmaCfg;\n\
void (* __xdata __at (memAddr+256+256) UserFunctionInterrupt)(void);\n\
";



