//
#pragma std_sdcc99
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"
//#include <mcs51/cc1110.h>
//#define nop __asm nop __endasm
//#define nop() nop
//
#define FOSC        24000000
#define FLF         (FOSC/750)
#define FSYS        (FOSC/2)
#define BoardFreq   32
//
#define sBit(byte,bit)     (byte|= (1<<bit))
#define cBit(byte,bit)     (byte&=~(1<<bit))
#define tBit(byte,bit)     (byte&  (1<<bit))
#define fBit(byte,bit)     (byte^= (1<<bit))
//
#define RedLedInit     sBit(P1DIR,1)
#define RedLedOn       sBit(P1,1)
#define RedLedOff      cBit(P1,1)
#define RedLedFlp      fBit(P1,1)
//
#define Gled_off()      nop
#define Gled_on()       nop
//
uint8_t butCnt,butState;
#define FBUTTON 8
#define ButInit   {cBit(P1DIR,7);cBit(P1INP,7);sBit(P1,7);cBit(P2INP,6);butCnt=128;butState=0;butCode=0;}
#define But       tBit(P1,7)
#define U_ButTest But
//#define W_CntInit {cbit(P1DIR,0);cbit(P1INP,0);sbit(P1,0);}
//#define W_Cnt      tbit(P1,0)
//// Triac
//#define ScrOn       sbit(P0,3)
//#define ScrOff      cbit(P0,3)
//// Driver
//#define LedOn       cbit(P1,0)
//#define LedOff      sbit(P1,0)

void ccInit(void);
void goToSleep(void);
void ParseButCode(uint8_t buttonCode);
void CheckButton(void);

//
void ProcessP2_isr(uint8_t flags);
void ProcessP0_isr(uint8_t flags);

void  FlashProgramming(void);
uint8_t MCS51(void);

//#define nop __asm nop __endasm
// Status Flags
//enum _BootState
//{
//    TAB,M,C,S,d5,d1,PROG
//} BootState;
// LED mask
uint8_t LedMask;
#define MaskOff   0x00
#define MaskFind  0x27
#define MaskWait  0x23
#define MaskNorma 0x01
#define MaskNoNet 0x03
//
void putchar(char c);
char getchar(void);
#define MAXUART 255
#define ENABLE_TX_UART()  { IEN2|= IEN2_UTX0IE;}
#define DISABLE_TX_UART() { IEN2&=~IEN2_UTX0IE;}
#define ENABLE_RX_UART()  { IEN0|= IEN0_URX0IE;} //     URX0IE=1;}
#define DISABLE_RX_UART() { IEN0&=~IEN0_URX0IE;} //{URX0IE=0;}
#define RTSBIT0              cBit(P1,3)          //P1_3
#define RTSBIT1              sBit(P1,3)
#define memAddr 0xF000
#define USER_CODE_BASE 0x2000
__xdata  __at (memAddr)     uint8_t uartRX[256];
__xdata  __at (memAddr+256) uint8_t uartTX[256];
//uint8_t rxIn,rxOut,txIn,txOut;
__xdata  __at (memAddr+256+256) uint16_t UFI;
__xdata  __at (memAddr+256+256) uint8_t param[64]; // user function parameters
__xdata  __at (memAddr+1024) uint8_t RamBuf[1024];
#define  userIntLow     param[0]    // user interrupt
#define  userIntHigh    param[1]
#define  Flags          param[2]
#define  RedLedMask     param[3]
#define  TicksMain      param[4]
#define  butCode        param[5]
//#define  butCnt         param[6]
#define  TicksAlarm     param[7]
#define  rxIn           param[8]
#define  rxOut          param[9]
#define  txIn           param[10]
#define  txOut          param[11]
//__xdata  __at (memAddr+256+256+12) uint8_t  *sBuf;
__xdata  __at (memAddr+256+256+12) uint8_t flashBuf[34];
__xdata  __at (memAddr+256+256+12+34) uint8_t flashCnt;
__xdata  __at (memAddr+256+256+12+34+1) uint16_t flashAddr;
__xdata  __at (memAddr+256+256+12+34+1+2) struct cc_dma_channel dmaCfg;
//
void (* __xdata __at (memAddr+256+256) UserFunctionInterrupt)(void);
void user_function(void);
//
#define Utx (1<<0)  // usb In(Tx) ready
#define Urx (1<<0)  // usb Out(Rx) ready
#define RxD (1<<1)  // uart RX ready
#define TxD (1<<2)  // uart Tx Busy
#define CmD (1<<3)  // uart Command
#define CdC (1<<4)  // CDC mode
#define PrG (1<<5)  // Flash Programming
#define FlU (1<<6)  // User Program
#define FlT (1<<7)  // rf Finding
//
#define eepromAddr     (31*1024)
#define EEPROM_SIZE 16
//const __at (eepromAddr) uint8_t  FlashData[EEPROM_SIZE]=
//{
//    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xff,0xFF,0xff,0xFF,0xFF,0xFF,0xFF,0xFF,0xff,0xff,
//};

