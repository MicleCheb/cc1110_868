//
uint8_t HexToBin(uint8_t ch);
uint8_t storeToFlash(void);
uint8_t hexParse(void);
void initFlashErased(uint8_t *page0_31);
uint8_t FlashIsErased(uint8_t *page0_31,uint8_t page);
//void initFlashErased(uint8_t *page0_31)
//{
//    uint16_t flash=0x0000;
//    uint8_t j,page; //c=*(__xdata uint8_t *)USER_CODE_BASE
//    for(j=0; j<4; j++) page0_31[j]=0xFF;
//    while(flash<0x8000)
//    {
//        if(*(__xdata uint8_t *)flash != 0xFF)
//        {
//            page=(((uint16_t)flash)>>10);
//            page0_31[page>>3] &= ~(1<<(page%8));
//            flash=page+1;
//            flash<<=10;
//        }
//        else ++flash;
//    }
//}
//
//uint8_t FlashIsErased(uint8_t *page0_31,uint8_t page)
//{
//    return (page0_31[page>>3] & (1<<(page%8)));
//}
//
uint8_t HexToBin(uint8_t ch)
{
    if((ch>='0') && (ch<='9')) return (ch-'0');
    if((ch>='A') && (ch<='F')) return (ch-'A'+10);
    if((ch>='a') && (ch<='f')) return (ch-'a'+10);
//    printf_tiny("%02X",ch);
    return ch;
}
//
uint8_t storeToFlash(void)
{
//    uint8_t j;
//    printf_tiny("%02u -> %04X : ",flashCnt,flashAddr);
//    for(j=0;j<flashCnt;j++)
//    printf_tiny("%02X",flashBuf[j]);
//    printf_tiny("\n");
//    flashCnt=recCnt;
//    flashAddr=recAdr;
    writeFlash();
    return 0;
}
#define CHECK_NO_ERASE 0
#define CHECK_AND_ERASE 1
void CheckFlash(uint8_t ERASE_FLASH)
{
    uint16_t flash=0x0000;
    uint8_t page;
//    initFlashErased(page0_31);
    puts("Check Flash : ");
    for(page=0; page<32; page++)
    {
        flashAddr=((uint16_t)page)<<10;
        for(flash=0; flash<1024; flash++)
        {
            if(*(__xdata uint8_t *)(flashAddr+flash) != 0xFF)
            {
                if((flashAddr >= USER_CODE_BASE) && (flashAddr < eepromAddr) )
                {
                    if(ERASE_FLASH)
                    {
                        eraseFlash();
                        putchar('F');
                    }
                    else  putchar('O');
                }
                else  putchar('L');
                flash=1025;
            }
        }
        if(flash==1024) putchar('F');
    }
    putchar('\n');
}
//
uint8_t hexParse(void)
{
    enum _HexParseState
    {
        hpColon,
        hpLength,
        hpAddress,
        hpRecord,
        hpData,
        hpChecksum,
        hpEndOfFile,
        hpEndHex
    } HexParseState;
#define recData     0   //:10246200464C5549442050524F46494C4500464C33
#define recEOF      1   //:00000001FF
#define recSegment  2   //:020000021200EA
#define recLinear   4   //:02000004FFFFFC
#define recMDKARM   5   //:04000005000000CD2A
    uint8_t ch,t;
    uint8_t tmpData=0,bytesLeft=0,recCnt=0;
//    uint32_t LinAdr;
    uint8_t  Type=0;
    uint8_t  CheckSum=0;
//    uint8_t page0_31[4];
    CheckFlash(CHECK_NO_ERASE);
//
    puts(" E - ERASE");
    puts(" P - PROGRAM");
    puts(" Q - REBOOT");
    puts(" S - BOOT ERASE&PROGRAM");
    while(1)
    {
        switch(getchar())
        {
        case 'E':
            puts("ERASING...");
            CheckFlash(CHECK_AND_ERASE);
            break;
        case 'P':
            puts("PROGRAMMING...");
            goto START_LOADER; //continue;
            break;
        case 'Q':
            IEN0&=~IEN0_EA;	//EA = 0;
            WDCTL = (1 << 3); // enable 1 sec
            while (1);
            break;
        case 'S':
            puts("Not implemented");
            return 7;
            break;
        default:
            return 8;
            break;
        }
    }
//
START_LOADER:
//
    HexParseState=hpColon;

    while(HexParseState!=hpEndHex)
    {
        ch = getchar();
        switch(HexParseState)
        {
        case   hpColon:
            if(ch==':')
            {
                HexParseState=hpLength;
                bytesLeft=2;
                flashCnt=0;
                CheckSum =0;
            }
            break;
        case  hpLength:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            flashCnt<<=4;
            flashCnt+=t;
            if( --bytesLeft == 0)
            {
                HexParseState=hpAddress;
                bytesLeft=4;
                CheckSum += flashCnt;
                flashAddr=0;
            }
            break;
        case   hpAddress:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            flashAddr<<=4;
            flashAddr+=t;
            if(--bytesLeft==0)
            {
                HexParseState=hpRecord;
                bytesLeft=2;
                Type=0;
                CheckSum+=flashAddr>>8;
                CheckSum+=flashAddr&0x00FF;
            }
            break;
        case   hpRecord:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            Type<<=4;
            Type+=t;
            if(--bytesLeft==0)
            {
                CheckSum+=Type;
                recCnt=0;
                tmpData=0;
                switch(Type)
                {
                case recData:
                    if(flashCnt)
                    {
                        HexParseState=hpData;
                        bytesLeft=2*flashCnt;
                    }
                    else
                    {
                        HexParseState= hpChecksum;
                        bytesLeft=2;
                    }
                    break;
                case recEOF:
                    HexParseState=hpEndOfFile;
                    bytesLeft=2;
                    break;
                case recSegment:
                    HexParseState=hpData;
                    bytesLeft=4;
                    break;
                case recLinear:
                    HexParseState=hpData;
                    bytesLeft=4;
                    break;
                case recMDKARM:
                    HexParseState=hpData;
                    bytesLeft=8;
                    break;
                default:
                    return 2;//goto HexError;
                    break;
                }
            }
            break;
        case   hpData:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            tmpData<<=4;
            tmpData+=t;
            if(bytesLeft%2)
            {
                flashBuf[recCnt++]=tmpData;
                CheckSum+=tmpData;
                tmpData=0;
            }
            if(--bytesLeft==0)
            {
                HexParseState= hpChecksum;
                tmpData=0;
                bytesLeft=2;
            }
            break;
        case   hpChecksum:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            tmpData<<=4;
            tmpData+=t;
            if(--bytesLeft==0)
            {
                CheckSum+=tmpData;
                if(CheckSum) return 3;//goto HexError;
                HexParseState= hpColon;
                if(Type != recData) break;
                if(flashAddr < USER_CODE_BASE) break;
//#define page  tmpData
//                page = flashAddr>>10;
//                if(!FlashIsErased(page0_31,page))
//                {
//                    eraseFlash();
//                    page0_31[page>>3] |= (1<<(page%8));
//                }
//                flashAddr+=flashCnt;
//                page = flashAddr>>10;
//                if(!FlashIsErased(page0_31,page))
//                {
//                    eraseFlash();
//                    page0_31[page>>3] |= (1<<(page%8));
//                }
//                flashAddr-=flashCnt;
                //                if(storeToFlash()) return 4;//goto HexError;
                writeFlash();
            }
            break;
        case   hpEndOfFile:
            if((t=HexToBin(ch))>15) return 1;//goto HexError;
            tmpData<<=4;
            tmpData+=t;
            if(--bytesLeft==0)
            {
                CheckSum+=tmpData;
                if(CheckSum) return 3;//goto HexError;
                HexParseState=  hpEndHex;//goto HexExit;
            }
            break;
        default:
            return 5;//goto HexError;
            break;
        }
    };
    return 0;
}
//
