/*

        puts(" B - Start UART LOADER");
        puts(" R - Start RF LOADER");

        puts(" S - Self LOADER");
        case 'B':
            RedLedMask=0x47;
            puts("Start UART LOADER");
            if(hexParse()) puts("\nError!");
            else puts("\nUART Done!");
            break;
        case 'R':
            RedLedMask=0x47;
            puts("Start RF LOADER");
            if(hexParse()) puts("\nError!");
            else puts("\nRF Done!");
            break;
                    case 'S':
            puts("Not implemented");
            break;



 */
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"
#define mcs51FLASHPAGE_SIZE 1024
#define memAddr 0xF000
#define USER_CODE_BASE 0x2000
extern __xdata  __at (memAddr+1024) uint8_t RamBuf[mcs51FLASHPAGE_SIZE];
extern __xdata  __at (memAddr+256+256+12) uint8_t flashBuf[34];
extern __xdata  __at (memAddr+256+256+12+34) uint8_t flashCnt;
extern __xdata  __at (memAddr+256+256+12+34+1) uint16_t flashAddr;
void eraseFlash(void);
//void writeFlash(uint16_t buf, uint16_t cnt, uint16_t flash_addr);
void writeFlash(void);
//
int8_t asciiToBin(void)
{
    int8_t ch;
    ch=getchar();
//    printf("\nch=%c",ch);
    if((ch>='0') && (ch<='9'))      ch-='0';
    else if((ch>='A') && (ch<='F')) ch-=('A'-10);
    else if((ch>='a') && (ch<='f')) ch-=('a'-10);
    else ch=-1;
//    printf("%02X",ch);
    return ch;
}
//
int16_t getData(void)
{
    int8_t rD1,rD0;
    if((rD1=asciiToBin())==-1)return -1;
    if((rD0=asciiToBin())==-1) return -1;
    return (rD1<<4)|rD0;
}
//
uint8_t IntelHex(__xdata uint8_t *buf)
{
//    enum _IntelHexState
//    {
//        ihColon,
//        ihLength,
//        ihAddress,
//        ihRecord,
//        ihData,
//        ihChecksum,
//        ihEndOfFile,
//        ihEndHex
//    } IntelHexState;
#define recData     0   //:10246200464C5549442050524F46494C4500464C33
#define recEOF      1   //:00000001FF
#define recSegment  2   //:020000021200EA
#define recLinear   4   //:02000004FFFFFC
#define recMDKARM   5   //:04000005000000CD2A
    int16_t t;
//    uint8_t tmpData=0,bytesLeft=0,recCnt=0;
    uint8_t  Length;
//    uint16_t hAddress;
//    uint8_t  hType;
    uint8_t  CheckSum;

    while(getchar() != ':');
    if((t=getData()) < 0) return 1;
    Length=t+2+1+1;
    *buf++=CheckSum=t;
    while( Length--)
    {
        if((t=getData()) < 0) return 1;
        *buf++ = t;
        CheckSum+=t;
    }
    return CheckSum;
}
//
//
uint8_t mcs51GetPage(uint8_t ch)
{
    if((ch>='0') && (ch<='9')) return (ch-'0');
    if((ch>='A') && (ch<='F')) return (ch-'A'+10);
    return (ch-'F'+16);
}

//
uint8_t MCS51(void)
{
    uint16_t i;
    uint8_t ch;
    __xdata  uint8_t *flashp;
    uint8_t page;
    printf("\n\rCC1110 Flash Loader\r\n");
    printf(" I - INFO\n");
    printf(" R - READ ALL\n");
    printf(" r - READ PAGE 0-9,A-F+\n");
    printf(" E - ERASE ALL\n");
    printf(" e - ERASE PAGE 0-9,A-F+\n");
    printf(" W - PROGRAM\n");
    printf(" B - BOOTLOADER\n");
    printf(" Q - QUIT\n");
//
    while((ch=getchar())!='Q')
    {
        switch(ch)
        {
        case 'I':
            printf("I-Chip ID %02X%02X\n", PARTNUM,VERSION );
            break;
        case 'E':
            printf("E-Erasing ALL...");
            flashAddr = USER_CODE_BASE;
            for(page=8; page<32; page++)
            {
                eraseFlash();
                flashAddr+=mcs51FLASHPAGE_SIZE;
            }
            printf("OK\n");
            break;
        case 'e':
            printf("e-Erasing page...");
            ch=getchar();
            page= mcs51GetPage(ch);
            printf("%u ",page);
            if(page>7 && page<32)
            {
                eraseFlash();
                flashAddr = mcs51FLASHPAGE_SIZE*page;
            }
            printf("OK\n");
            break;
        case 'W':
////                mcs51WriteMemory(0xFF00+64,(uint16_t)&prgCode,sizeof(prgCode));
//            mcs51WriteMemory(0xFF00+64*2,(uint16_t)&mcs51CPUWriteFlash,34);
            printf("W-Write...");
            do
            {
                ch=IntelHex(RamBuf);
                if(ch)
                {
                    printf("Hex ERROR\n");
                    return 1;
                }
                else
                {
                    if(RamBuf[3]==recEOF)
                    {
                        printf("OK\n");
                        goto quit;
                        break;//ch=255;
                    }
                    else if(RamBuf[3]==recData && RamBuf[0]>0)
                    {
                        flashCnt=RamBuf[0];
                        flashAddr=(RamBuf[1]<<8)|RamBuf[2];
                        for(i=0; i<flashCnt; i++) flashBuf[i]=RamBuf[i+4];
                        writeFlash();
                    }

//                        mcs51WriteFlash((RamBuf[1]<<8)|RamBuf[2],RamBuf+4,RamBuf[0]);
                };
            }
            while(ch==0);
            break;
        case 'R':
            printf("R-Read MEMORY\n");
            flashp=0;
            for(page=0; page<32; page++)
            {
                printf("\n@%04X",page*mcs51FLASHPAGE_SIZE);
//                mcs51ReadMemory(mcs51FLASHPAGE_SIZE*page,RamBuf,mcs51FLASHPAGE_SIZE);
                for (i=0; i<mcs51FLASHPAGE_SIZE; i++)
                {
                    if(i%16==0) printf("\n");
                    ch=*flashp++;
                    printf("%02X ",ch);
                }
//                printf("\n");
            }
            printf("\nq\nOK\n");
            break;
        case 'r':
            printf("r-Read page...");
            ch=getchar();
            page= mcs51GetPage(ch);
            printf("%u\n",page);
//            mcs51ReadMemory(mcs51FLASHPAGE_SIZE*page,RamBuf,mcs51FLASHPAGE_SIZE);
            flashp=mcs51FLASHPAGE_SIZE*page;
            for (i=0; i<mcs51FLASHPAGE_SIZE; i++)
            {
                if(i%32==0) printf("\n");
                ch=*flashp++;
                printf("%02X",ch);
            }
//            printf("\n");
            printf("\nOK\n");
            break;
        case 'B':
//            return  BootLoader();
//printf(" B - BOOTLOADER\n");
            printf("Not implemented\n");
            break;
        //        case 'Q':
//            RST = YES;    //0;
//            delay(100);
//            RST = NONE;
//            return 2;
//            break;
        default:
            break;
        }
    }
quit:
//    RST = YES;    //0;
//    delay(100);
//    RST = NONE;
    return 0;
}
//
