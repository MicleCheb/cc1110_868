//
void initSleep(void);
void initUart(void);
void ccInit(void)
{
    SLEEP = 0;              // HS XTAL 48MHz + HSRC
    CLKCON = (1<<7)|(1<<3)|(1<<0);        // LFRC 32kHz
    while( !(SLEEP&(1<<6)));// HSXT
    while( !(SLEEP&(1<<5)));// HSRC
    SLEEP |= (1<<2);        // HSRC OFF
//
    RedLedInit;
    RedLedOn;
    RedLedMask=MaskNorma;
    LedMask=1;
//    Gled_on();
    ButInit;
    initUart();
    initSleep();
}
//
void initSleep(void)
{
    uint8_t t;
//  Setup interrupt
// Clear interrupt flags
// Clear [IRCON.STIF] (Sleep Timer CPU interrupt flag)
//    STIF = 0;
//    // Clear [WORIRQ.EVENT0_FLAG] (Sleep Timer peripheral interrupt flag)
//    cbit(WORIRQ,0);  // WORIRQ_EVENT0_FLAG;
//    // Set individual interrupt enable bit in the peripherals SFR
//    sbit(WORIRQ,4); // WORIRQ_EVENT0_MASK;    // Enable interrupt mask for sleep timer
//    // Set the individual, interrupt enable bit [IEN0.STIE=1]
//    STIE = 1;
////Now the time between two consecutive Event 0s is decided by:
//       t = EVENT0 * 2^(5*WOR_RES) / 32768
//       By using EVENT0 = 32 and WOR_RES = 2, t = 1 s. So by using these values,
//       a Sleep Timer interrupt will be generated every second.
// Set [WORCTL.WOR_RES = 1]
//    WORCTRL |= 0;
//Must wait for 2 clock periods after resetting the Sleep Timer before
//setting EVENT0 value
// Reset timer and set EVENT0 value.
#define WOR_RES 0
    WORCTRL = 4 + WOR_RES;    //WORCTL_WOR_RESET; 32 kHz Clock
    t = WORTIME0;
    while(t == WORTIME0);   // Wait until a positive 32 kHz edge
    t = WORTIME0;
    while(t == WORTIME0);   // Wait until a positive 32 kHz edge
    WOREVT0 = (FLF/(1<<(5*WOR_RES))/BoardFreq)&0x00FF;    // 8 Herz freq
    WOREVT1 = (FLF/(1<<(5*WOR_RES))/BoardFreq)>>8;
    cBit(IRCON,7);//STIF = 0;
    WORIRQ &=~(1<<0);
    WORIRQ |= (1<<4);
    IEN0|=IEN0_STIE;//STIE = 1;
// Select Power Mode 1 (SLEEP.MODE = 0).
//    SLEEP |= 0;
    TicksMain=0;
//    TicksAlarm=0;
    Flags=0;
//The system will now wake up when Sleep Timer interrupt occurs. When awake,
//       the system will start enter/exit Power Mode 0 using the loop below.
}
//
void goToSleep(void)
{
//    register
    uint8_t j;
// Alignment of entering PM{0 - 2} to a positive edge on the 32 kHz clock source
    j = WORTIME0;
    while(j == WORTIME0);    // Wait until a positive 32 kHz edge
//    j = WORTIME0;
//    while(j == WORTIME0);
// SLEEP = 0x04+1;
    PCON |= 1;          // Go into Power Mode
    nop;
}
//
void delay (uint8_t milliseconds)
{
    uint8_t j,k;
    j=WORTIME0;
    while (--milliseconds)
    {
        k=(FLF/1000);
        while(k--)
        {
            while(j==WORTIME0);
            j=WORTIME0;
        }
    }
}
//
uint8_t check_for_payload()
{
    if (*((__xdata uint8_t*)USER_CODE_BASE) == 0xFF)
        return 0;
    else
        return 1;
}
//
struct _CDC_LINE_CODING
{
    uint32_t dwDTERate;       /* Data terminal rate */
    uint8_t  bStopBits;       /* Number of stop bits */
    uint8_t  bParityType;     /* Parity bit type */
    uint8_t  bDataBits;       /* Number of data bits */
};
#define CDC_CHAR_FORMAT_1_STOP_BIT     0
#define CDC_CHAR_FORMAT_1_5_STOP_BIT   1
#define CDC_CHAR_FORMAT_2_STOP_BIT     2

#define CDC_PARITY_TYPE_NONE           0
#define CDC_PARITY_TYPE_ODD            1
#define CDC_PARITY_TYPE_EVEN           2
#define CDC_PARITY_TYPE_MARK           3
#define CDC_PARITY_TYPE_SPACE          4
void uartSet(void)
{
    uint32_t b;
    uint8_t k;
//

    /*
    Where
    dwDTERate	Baudrate in bits per second.
    bCharFormat	Number of stop bits (0 = 1 stop bit, 1 = 1.5 stop bits, 2 = 2 stop bits).
    bParityType	Parity type (0 = None, 1 = Odd, 2 = Even, 3 = Mark, 4 = Space).
    bDataBits	Number of data bits (5, 6, 7, 8, or 16).
    bState Serial State     bit 0: DTR state
                            bit 1: RTS state
    */
    struct _CDC_LINE_CODING  CDC_LINE_CODING = {57600, 0, 0, 8};
//
    U0CSR  = UxCSR_MODE_UART|UxCSR_RE; // UART mode + Receiver enabled
    U0UCR  = UxUCR_FLOW_DISABLE|UxUCR_STOP_HIGH|UxUCR_BIT9_8_BITS; //UxUCR_FLUSH|
    if(CDC_LINE_CODING.bDataBits==9)   U0UCR  |= UxUCR_BIT9_9_BITS;
    if(CDC_LINE_CODING.bStopBits==CDC_CHAR_FORMAT_2_STOP_BIT) U0UCR  |= UxUCR_SPB_2_STOP_BITS;
    if(CDC_LINE_CODING.bParityType==CDC_PARITY_TYPE_ODD)
    {
        U0UCR  |=UxUCR_PARITY_ENABLE|UxUCR_D9_ODD_PARITY| UxUCR_BIT9_9_BITS;
    }
    else if(CDC_LINE_CODING.bParityType==CDC_PARITY_TYPE_EVEN)
    {
        U0UCR  |=UxUCR_PARITY_ENABLE|UxUCR_D9_EVEN_PARITY| UxUCR_BIT9_9_BITS;
    }
    else
    {
        U0UCR  |=UxUCR_PARITY_DISABLE;
    }
//    |UxUCR_FLOW_DISABLE|UxUCR_PARITY_DISABLE|UxUCR_SPB_1_STOP_BIT|UxUCR_BIT9_8_BITS;
    //|(1 << 7) | (0 << 6) | (1 << 1); // flush no RTS-CTS flow UxUCR|  # define UxUCR_D9_EVEN_PARITY           (0 << 5)
//# define UxUCR_D9_ODD_PARITY            (1 << 5)
#define U_E (LOG2(BAUD*2^28/FSYS/256))
#define U_M (2^28*BAUD/FSYS/2^U_E - 256)
    b=CDC_LINE_CODING.dwDTERate;
    k=0;
    while(b < FSYS)
    {
        k++;
        b<<=1;
    };
    U0GCR  = (20-k);   //14; // 24MHz : 16
    U0BAUD =(b/(FSYS>>8))&0x000000FF;   //163;
//    CDC_LINE_CODING.bState=3;
    U0DBUF=0x0A;
}
void initUart(void)
{
    /*
    USART1 ALT1 Rx   Tx   Rt   Ct
                P0_5 P0_4 P0_3 P0_2
    */
//#define BAUD 115200
// UART0 BAUD,8,1,N
// PORT1 4 Rx  in
//       5 Tx  out
//       2 Ct  in
//       3 Rt  out
//    P0SEL |= (1<<5)|(1<<4);
//    P0DIR |= (1<<4);
//// Configure UART0,UART1 for Alternative 2 =>
//// T4 Alt1 USART0,UART1 Alt2
//    PERCFG = PERCFG_U1CFG_ALT_1|PERCFG_U0CFG_ALT_2;//(1 << 1) | (1 << 0);
    P1SEL |= (1<<5)|(1<<4);
    P1DIR |= (1<<5)|(1<<3);
    RTSBIT0;//cBit(RTSBIT);//     RTSBIT=0;
//         PERCFG = (1 << 1) | (1 << 0);
    PERCFG = PERCFG_U0CFG_ALT_2|PERCFG_U1CFG_ALT_1;//(1 << 1) | (1 << 0);
    uartSet();
// UART buffer Init
    rxIn=rxOut=txIn=txOut=0;
    do
    {
        uartRX[rxIn]=uartTX[rxIn]=0;
    }
    while(--rxIn);
//    printf_tiny("Hello\n");
//U1DBUF=10;
    ENABLE_RX_UART(); // USART0 RX interrupt enable
    DISABLE_TX_UART();// USART0 TX interrupt disable
}
//
