#include "CC1110Modem868.h"
#include "ccInit.h"
#include "ccInterrupts.h"
#include "ccUtil.h"
#include "ccFlash.h"
#include "MCS51Flash.h"
//
//void nouserFunc(void)
//{
//    nop;
////    RedLedFlp;
////    printf_tiny("nouserFunc = %u\n",TicksMain);
//}
//void (* const fp)(void) = (void (*)(void))0x1400;
//fp();
void checkUser(void)
{
    uint8_t c;
    c=*(__xdata uint8_t *)USER_CODE_BASE;
    if( c != 0xFF )
    {
        puts("\nStart USER PROGRAM");
        Flags=FlU;
        flashCnt=0;
        flashAddr=0;
        RedLedMask=MaskNorma;
//            __asm lcall USER_CODE_BASE __endasm;
        __asm ljmp #(USER_CODE_BASE) __endasm;
    }
    else
    {
        RedLedMask=MaskWait;
        puts("\nNo USER PROGRAM!");
        TicksAlarm=16;
    }
}
//
void main(void)
{
    UFI=0;  //(uint16_t)&nouserFunc; //0;//
    ccInit();
//    BootState=TAB;
//    initRadio();
    IEN0|=IEN0_EA;	//EA=1;
    puts("\nCC1110 RFModem 868 MHz");

    RedLedMask=MaskWait;
//    FlashProgramming();
    TicksAlarm=255;
    Flags=CmD;
    while(1)
    {

        puts("\n U - Start USER PROGRAM");
        puts(" F - Start Flash Loader");
        puts(" H - Help");
        puts(" Q - Ctrl-Q REBOOT");
        switch(getchar())
        {
        case 'U':
            checkUser();
            break;
        case 'F':
            Flags=PrG;
            printf(" Flash Write Error: %u\n",MCS51());
            Flags&=~PrG;
            break;
        case 'H':
            puts(Help);
            break;
        case 'Q': /* Soft RESET  quit */
            IEN0&=~IEN0_EA;	//EA = 0;
            WDCTL = (1 << 3); // enable 1 sec
            while (1);
            break;
        default:
            break;
        }
        goToSleep();
        if(!TicksAlarm) checkUser();
    }
}
