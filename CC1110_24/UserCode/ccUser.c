//
#include <stdint.h>
#include <stdio.h>

//#pragma codeseg userfunc
// Linker options --xram-loc 0xF000 -Wl -buserfunc=0x2000
//const char hello[]="Hello,world!\n";
void putchar(char c);
char getchar(void);
#define MAXUART 255
#define ENABLE_TX_UART() {IEN2|=IEN2_UTX0IE;}
#define DISABLE_TX_UART() {IEN2&=~IEN2_UTX0IE;}
#define ENABLE_RX_UART() {URX0IE=1;}
#define DISABLE_RX_UART() {URX0IE=0;}
#define memAddr 0xF000
__xdata  __at (memAddr)     uint8_t uartRX[256];
__xdata  __at (memAddr+256) uint8_t uartTX[256];
//uint8_t rxIn,rxOut,txIn,txOut;
__xdata  __at (memAddr+256+256) uint16_t UFI;
__xdata  __at (memAddr+256+256) uint8_t param[64]; // user function parameters
#define  userIntLow     param[0]    // user interrupt
#define  userIntHigh    param[1]
#define  Flags          param[2]
#define  RedLedMask     param[3]
#define  TicksMain      param[4]
#define  butState       param[5]
#define  butCode        param[6]
#define  TicksAlarm     param[7]
#define  rxIn           param[8]
#define  rxOut          param[9]
#define  txIn           param[10]
#define  txOut          param[11]
void UserInt(void);
void hexPrint(uint8_t c);
//
void putchar(char c)
{
    while(txIn==(txOut-1));
    uartTX[txIn++]=c;
    ENABLE_TX_UART();
//    U0DBUF=c;
//    while (!(U0CSR&UxCSR_TX_BYTE)) ; // TX_BYTE
//    U0CSR&=~UxCSR_TX_BYTE;
}
//
char getchar(void)
{
//    if(rxOut==rxIn) return 0;
//    else return uartRX[rxOut++];
    TicksAlarm=30;
    while(rxOut==rxIn)
    {
        if(!TicksAlarm) return 0;
    };
    return uartRX[rxOut++];
}
//
void user_function(void)
{
//  uint16_t storeInt;
//    EA=0;
//    storeInt=UFI;
//    UFI=(uint16_t)&UserInt;
//    EA=1;
//
    TicksAlarm=2;
    while(TicksAlarm);
    {
//        UserInt();
        printf("End %u\n",TicksMain);
        nop;
//        hexPrint(TicksMain);
    }
//
//    EA=0;
//    UFI=storeInt;
//    EA=1;
}
void UserInt(void)
{
//    printf("User Time = %u\n",TicksMain);
    nop;
}

