#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"
#include "miscUtil.h"

#define BoardFreq   32
#define FOSC        24000000
#define FLF         (FOSC/750)
#define FSYS        (FOSC/2)
#define devAddress  2       // dev Address
#define MAXPACKET   32      // max packet length with 2bytes rssi+lqi
#define RadioPause  3*BoardFreq       // 3 seconds

#define XT2ON   {SLEEP=0;while(!tBit(SLEEP,6));CLKCON&=~(1<<6);}
#define XT2OFF  {CLKCON|=(1<<6);SLEEP=0x04;}
void Init868(void );
void goToSleep(void);
void FreqConf(uint32_t FCarrier);
void rfSend868(__xdata uint8_t *buf,__xdata uint8_t txBytes);// __reentrant;
uint8_t rfRcv868(__xdata uint8_t *buf,__xdata uint8_t rxBytes);// __reentrant;
//
//#define nop	__asm nop __endasm;
#define MaskOff   0x00
#define MaskFind  0x27
#define MaskWait  0x23
#define MaskNorma 0x01
#define MaskNoNet 0x03
//
//void putchar(char c);
//char getchar(void);
#define MAXUART 255
//    IEN0|=IEN0_EA;	//EA=1;
//            IEN0&=~IEN0_EA;	//EA = 0;
#define ENABLE_TX_UART() {IEN2|=IEN2_UTX0IE;}
#define DISABLE_TX_UART() {IEN2&=~IEN2_UTX0IE;}
#define ENABLE_RX_UART()  { IEN0|= IEN0_URX0IE;} //     URX0IE=1;}
#define DISABLE_RX_UART() { IEN0&=~IEN0_URX0IE;} //{URX0IE=0;}
#define memAddr 0xF000
#define USER_CODE_BASE 0x2000
__xdata  __at (memAddr)     uint8_t uartRX[256];
__xdata  __at (memAddr+256) uint8_t uartTX[256];
//uint8_t rxIn,rxOut,txIn,txOut;
__xdata  __at (memAddr+256+256) uint16_t UFI;
__xdata  __at (memAddr+256+256) uint8_t param[64]; // user function parameters
#define  userIntLow     param[0]    // user interrupt
#define  userIntHigh    param[1]
#define  Flags          param[2]
#define  RedLedMask     param[3]
#define  TicksMain      param[4]
#define  butCode        param[5]
#define  butCnt         param[6]
#define  TicksAlarm     param[7]
#define  rxIn           param[8]
#define  rxOut          param[9]
#define  txIn           param[10]
#define  txOut          param[11]
//__xdata  __at (memAddr+256+256+12) uint8_t  *sBuf;
__xdata  __at (memAddr+256+256+12) uint8_t flashBuf[34];
__xdata  __at (memAddr+256+256+12+34) uint8_t flashCnt;
__xdata  __at (memAddr+256+256+12+34+1) uint16_t flashAddr;
__xdata  __at (memAddr+256+256+12+34+1+2) struct cc_dma_channel dmaCfg;







void (* __xdata __at (memAddr+256+256) UserFunctionInterrupt)(void);
void user_function(void);
//
#define Utx (1<<0)  // usb In(Tx) ready
#define Urx (1<<0)  // usb Out(Rx) ready
#define RxD (1<<1)  // uart RX ready
#define Rf8 (1<<2)  // Radio 868
#define CmD (1<<3)  // uart Command
#define CdC (1<<4)  // CDC mode
#define PrG (1<<5)  // Flash Programming
#define FlU (1<<6)  // User Program
#define FlT (1<<7)  // rf Finding
//
#define eepromAddr     (31*1024)
#define EEPROM_SIZE 16
//const __at (eepromAddr) uint8_t  FlashData[EEPROM_SIZE]=
//{
//    0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xff,0xFF,0xff,0xFF,0x08,0xFF,0xFF,0xFF,0xff,0xff,
//};
#define USER_CODE_BASE 0x2000
#define USERMEM 0xF400
__xdata __at(USERMEM) uint8_t  tx868[MAXPACKET];
__xdata __at(USERMEM+MAXPACKET) uint8_t rx868[MAXPACKET];
//__xdata uint8_t  tx169[MAXPACKET];
//__xdata uint8_t  rx169[MAXPACKET];
__xdata __at(USERMEM+MAXPACKET*2)   uint8_t pTxIn;
__xdata __at(USERMEM+MAXPACKET*2+1) uint8_t pTxOut;
__xdata __at(USERMEM+MAXPACKET*2+2) uint8_t pRxIn;
__xdata __at(USERMEM+MAXPACKET*2+3) uint8_t pRxOut;
__xdata __at(USERMEM+MAXPACKET*2+4) uint8_t rxCnt;
__xdata __at(USERMEM+MAXPACKET*2+5) uint8_t txCnt;
//__xdata uint8_t rxBuf[PACKETMAX],txBuf[PACKETMAX];
__xdata __at(USERMEM+MAXPACKET*2+8) uint8_t aesKey[16];
__xdata __at(USERMEM+MAXPACKET*2+24) uint8_t paramList[8];
union __param
{
    uint32_t p32[1];
    uint16_t p16[2];
    uint8_t  p8[4];
}  __xdata __at(USERMEM+MAXPACKET*2+32) pOR;



void delay (unsigned char n);
void UserInt(void);
//__xdata
//uint8_t RadioTimer;//,BeaconTimer;
#define RadioTimer  TicksAlarm
#define BeaconTimer TicksAlarm
void UserInt(void)
{
//    uint8_t ch;
//    ch=getchar();
//    if(ch) putchar(ch);//printf("User Char = %02X\n",ch);//
//if((TicksMain%17)==0)    printf("User Time = %u\n",TicksMain);
//    if(RadioTimer) --RadioTimer;
//    if((TicksMain%BoardFreq)==0)
////    {
//        if(--BeaconTimer == 0)
//        {
//            BeaconTimer=RadioPause*BoardFreq;
//        }
//
//    }
//    nop;
//    P1_1 ^= 1;
}
//
void putchar(char c)
{
//    uint8_t wait;

    uartTX[txIn++]=c;
//    ENABLE_TX_UART();
//    do
//    {
//        wait=(txIn-txOut);
//    }
//    while(wait>=MAXUART);
//    U0DBUF=c;
//    while (!(U0CSR&UxCSR_TX_BYTE)) ; // TX_BYTE
//    U0CSR&=~UxCSR_TX_BYTE;
}
//
char getchar(void)
{
    while(rxOut==rxIn);
    return uartRX[rxOut++];
//    TicksAlarm=30;
//    while(rxOut==rxIn)
//    {
//        if(!TicksAlarm) return 0;
//    };
//    return uartRX[rxOut++];
}
//
void goToSleep(void)
{
//    register
    uint8_t j;
// Alignment of entering PM{0 - 2} to a positive edge on the 32 kHz clock source
    j = WORTIME0;
    while(j == WORTIME0);    // Wait until a positive 32 kHz edge
//    j = WORTIME0;
//    while(j == WORTIME0);
// SLEEP = 0x04+1;
    PCON |= 1;          // Go into Power Mode
    nop;
}
////
//void delay (uint8_t milliseconds)
//{
//    uint8_t j,k;
//    j=WORTIME0;
//    while (--milliseconds)
//    {
//        k=(FLF/1000);
//        while(k--)
//        {
//            while(j==WORTIME0);
//            j=WORTIME0;
//        }
//    }
//}
//
//
void delay (unsigned char n)
{
    TicksAlarm=n;
    while(TicksAlarm);
}
//
uint8_t ToHex(uint8_t ch)
{
    if(ch<10) ch+='0';
    else ch+=('A'-10);
    return ch;
}
void HexPrt(__xdata uint8_t *buf,uint8_t cnt);
void HexPrt(__xdata uint8_t *buf,uint8_t cnt)
{
//    uint8_t ch;
//    putchar('\n');
    printf("\n");
    while(cnt--)
    {
//        ch=*buf++;
//        putchar(ch>>4);
//        putchar(ch&0x0F);
        printf("%02X",*buf++);
    };

    printf("\n");
//    putchar('\n');
}
//
#define BaseFreq    868950000   // 868,1 MHz
#define SyncWord    0xF68D
//
//void FreqConf868(uint32_t FCarrier)
//{
//    FREQ2=(FCarrier/FOSC);
//    FCarrier  -=  ((uint32_t)FOSC)*FREQ2;
//    FCarrier <<= 8;
//    FREQ1      = FCarrier/FOSC;
//    FCarrier  -=  ((uint32_t)FOSC)*FREQ1;
//    FCarrier <<= 8;
//    FREQ0      = FCarrier/FOSC;
//}
/* RF settings SoC: CC1110 Freq=868.3 MHz Baud=100 kHz
# Sync word qualifier mode = 16/16 + carrier-sense above threshold
# Channel spacing = 199.951172
# Data rate = 99.9756
# RX filter BW = 101.562500
# PA ramping = false
# Preamble count = 8
# Whitening = false
# Address config = No address check
# Carrier frequency = 868.299866
# Device address = 0
# TX power = 0
# Manchester enable = false
# CRC enable = false
# Deviation = 44.433594
# Packet length mode = Fixed packet length mode. Length configured in PKTLEN register
# Packet length = 30
# Modulation format = GFSK
# Base frequency = 868.299866
# Modulated = true
# Channel number = 0
*/
void Init868(void)
{
    /* RF settings SoC: CC1110 */
    SYNC1     = 'K'; // sync word, high byte
    SYNC0     = 'V'; // sync word, low byte
    PKTLEN    = 0xFF; // packet length
    PKTCTRL0  = 0x00; // PKTLEN
    PKTCTRL1  = (4<<5); // PQT
    FSCTRL1   = 0x04; // frequency synthesizer control
    FREQ2     = 36;//0x21; // frequency control word, high byte
    FREQ1     = 45;//0x65; // frequency control word, middle byte
    FREQ0     = 221;//0x6A; // frequency control word, low byte
    MDMCFG4   = 10+(3<<4)+(2<<6);//0xCB; // modem configuration
    MDMCFG3   = 163;//0xF8; // D_RATE_E
    MDMCFG2   = 0x16; // 16/16 + carrier-sense above threshold
    MDMCFG1   = 3+(4<<4); // 8 bytes preamble
    MDMCFG0   = 17; // channel sp
    DEVIATN   = 6+(3<<4); // modem deviation setting
    MCSM0     = 0x18; // main radio control state machine configuration
    MCSM1     = 0x00;
    FOCCFG    = 0x16; // frequency offset compensation configuration
    AGCCTRL2  = 0x42; // agc control
    AGCCTRL1  = 0x40; // agc control
    FSCAL3    = 0xEA; // frequency synthesizer calibration
    FSCAL2    = 0x2A; // frequency synthesizer calibration
    FSCAL1    = 0x00; // frequency synthesizer calibration
    FSCAL0    = 0x1F; // frequency synthesizer calibration
    TEST1     = 0x31; // various test settings
    TEST0     = 0x09; // various test settings
    PA_TABLE0 = 0x50; // pa power setting 0
//    LQI       = 0x7F; // demodulator estimate for link quality
    RFST = RFST_SIDLE;
//      Wait for calibration to complete
    while (MARCSTATE != 0x01);
    cBit(IEN0, 0);
//      RF TX/RX done interrupt disable
    cBit(IEN2, 0);
    RFIM = 0;
    RFIF = 0;
    S1CON = 0; // Clear interrupt flag for SFD in RFIF
    cBit(TCON, 1);
//
//    RadioTimer=0;
//    BeaconTimer=RadioPause;
}
/// receive packet from RF (maxPacket bytes )
uint8_t  rfRcv868(__xdata uint8_t *rxBuffer,__xdata uint8_t rxBytes) //__reentrant
{
//    int8_t rssi=-128;
//    uint8_t cnt=rxBytes;
    XT2ON;
    MCSM0 &= ~RF_MCSM0_CLOSE_IN_RX_18DB;
//MCSM0 |=  0x03;
//    MCSM0 = 0x18;
    PKTLEN = rxBytes;
    RFST = RFST_SRX; //    SRX();
// CS detect
    while(RadioTimer && !tBit(RFIF, 0))
    {
        if(tBit(PKTSTATUS,6)) // CarrierSense  && rssi==-128
        {
//            rssi = RSSI;
            if(RSSI>32 && RSSI<128) MCSM0 |= RF_MCSM0_CLOSE_IN_RX_18DB;
        }
    }
//
    if(!RadioTimer)
    {
        if(tBit(PKTSTATUS,6))
        {
            RFST = RFST_SIDLE;
            return 255;
        }
        else
        {
            RFST = RFST_SIDLE;
            return 0;
        }
    }
// SYNC detecting, receive Data maxPacket byte
    while(tBit(PKTSTATUS,6) && rxBytes) //
    {
        if(tBit(TCON, 1))
        {
            cBit(TCON, 1);
            *rxBuffer++ = RFD;
            --rxBytes;
//PKTSTATUS SFD  3 CS 6
        }
    };
// wait idle
    RFST = RFST_SIDLE;
    while(MARCSTATE != 0x01);
    cBit(TCON, 1);
    RFIF=0;
    S1CON=0;
//    XT2OFF;
    *rxBuffer++ = RSSI;
    *rxBuffer++ = LQI;
    return (PKTLEN-rxBytes);
}
/// send packet to RF
void rfSend868(__xdata uint8_t *txBuffer,__xdata uint8_t txBytes) //__reentrant
{
//uint8_t cnt=txBytes;
    XT2ON;
    PKTLEN = txBytes;
    RFST = RFST_STX;
    while (txBytes)
    {
        if(tBit(TCON, 1))
        {
            cBit(TCON, 1);
            RFD = *txBuffer++;
            --txBytes;
        };
    };
    while(MARCSTATE != 0x01); // IDLE
    cBit(TCON, 1);
    RFIF=0;
    S1CON=0;
//    XT2OFF;
}
//

