#include <stdint.h>
#include <stddef.h>
#include "miscUtil.h"



//#define L_BLOCK0 4
//#define L_BLOCK1 14
//#define PACKED_CNT(X) (X+(X+L_BLOCK1-1)/L_BLOCK1)
//#define UNPACKED_CNT(X)  (X-(X+L_BLOCK1)/(1+L_BLOCK1) )
//#define DATA_MAX 14
//#define PACKETMAX (L_BLOCK0+1+PACKED_CNT(DATA_MAX))
//
#define bSEND	    0   //0-запрос параметра,1-передача
#define bREPLY	    1   //0-запрос,1-ответ
#define bDEVICE	    2   //0-от базы, 1- от ОУ
#define bAES	    3   //0-без шифрования,1-да
#define bSYNC	    4   //1-синхронизация сети
#define bNORETR	    5   //0-ретранслировать,1-нет
#define bRETR	    6   //1-пакет ретранслирован (не передаётся)
#define bDONE	    7   //1-запрос завершён (не передаётся)

#define cmdSEND	    (1<<0)  //0-запрос параметра,1-передача
#define cmdREPLY	(1<<1)   //0-запрос,1-ответ
#define cmdDEVICE	(1<<2)   //0-от базы, 1- от ОУ
#define cmdAES	    (1<<3)   //0-без шифрования,1-да
#define cmdSYNC	    (1<<4)   //1-синхронизация сети
#define cmdNORETR	(1<<5)   //0-ретранслировать,1-нет
#define cmdRETR	    (1<<6)   //1-пакет ретранслирован (не передаётся)
#define cmdDONE	    (1<<7)   //1-запрос завершён (не передаётся)

//
// rtrFunction
//
#define netGRP        0
#define netADR        1
#define netSYNC       2
#define netFREQ       3
#define netBAUD       4
#define netDEVIATN    5
#define netPOWER      6
#define netOFFSET     7
#define netRSSI       8
#define netLQI        9
#define netVBAT       10
#define netTIME       11
#define netTICK       12

//
#define ECCMAX 15
#define Poly8 0x2F
//
/*
struct status_type {
  unsigned delta_cts: 1;
  unsigned delta_dsr: 1;
  unsigned tr_edge:   1;
  unsigned delta_rec: 1;
  unsigned cts:       1;
  unsigned dsr:       1;
  unsigned ring:      1;
  unsigned rec_line:  1;
} status;
*/
#define L_BLOCK0 4
#define L_BLOCK1 14
#define PACKED_CNT(X) (L_BLOCK0+1+(X-L_BLOCK0)+((X-L_BLOCK0)+L_BLOCK1-1)/L_BLOCK1)
#define UNPACKED_CNT(X)  (X-(X+L_BLOCK1)/(1+L_BLOCK1) )
#define DATA_MAX 14
#define PACKETMAX (L_BLOCK0+1+PACKED_CNT(DATA_MAX))

uint8_t rtrDecrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key);
uint8_t rtrEncrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key);
uint8_t rtrPack(__xdata uint8_t *buf,uint8_t cnt);
uint8_t rtrUnPack(__xdata uint8_t *buf,uint8_t cnt);
uint8_t rtrCheck(__xdata uint8_t *buf,uint8_t cnt);
uint8_t rtrECC(__xdata uint8_t *buf,uint8_t cnt);
uint8_t rtrCRC8(__xdata uint8_t *buf,uint8_t cnt);

uint8_t rtrPrintPacket(__xdata uint8_t *pData,uint8_t pCnt);
uint8_t rtrSaveParam(__xdata uint8_t *buf,uint8_t cnt);
uint8_t rtrCreate(__xdata uint8_t *buf,uint8_t cmd,__xdata uint8_t *pList);
uint8_t rtrAddParam(__xdata uint8_t *pData,uint8_t pAdr,uint8_t cmd);
uint8_t rtrParse(__xdata uint8_t *RXbuf,uint8_t cnt,__xdata uint8_t *TXbuf);
uint8_t rtrCopyData(__xdata uint8_t *Dst,__xdata uint8_t *Src,uint8_t cnt);

uint32_t rtrComputeFREQ(uint32_t FCarrier);
//
struct rtrPacket
{
//    uint8_t rtrGrp;
    uint8_t rtrAdr;
//    uint8_t rtrDstAdr;
////    uint8_t rtrDstGrp;
//    uint8_t rtrSrcAdr;
//    uint8_t rtrSrcGrp;
    uint8_t rtrStartTime;
    uint8_t rtrCmd;
    uint8_t rtrDataLength;
//    uint8_t Crc8;
    uint8_t rtrData[DATA_MAX];
};
//
struct _rtrData
{
    uint8_t ParamType;
    uint8_t *ParamData;
};
//
uint8_t rtrSaveParam(__xdata uint8_t *buf,uint8_t cnt)
{
    struct rtrPacket *rP=(struct rtrPacket *)buf;
    uint8_t pData=offsetof(struct rtrPacket,rtrData);
    uint8_t  pCnt=rP->rtrDataLength;
    uint8_t pTotal=0;
    cnt=MIN(cnt,pCnt);
    if(cnt<=L_BLOCK0) return 0;
    cnt-=L_BLOCK0;
    while(cnt-- >1)
    {
        ++pTotal;
        switch(buf[pData++])
        {
        case netADR:
//            param.p8[0]=devAddress;
            rtrCopyData((uint16_t)&pOR,buf+pData,1);
            ++pData;
            --cnt;
            break;
        case netSYNC:
//            param.p16[0]=SyncWord;
            if(cnt>=2)
            {
                rtrCopyData((uint16_t)&pOR,buf+pData,2);
                pData+=2;
                cnt-=2;
                printf("SYNC=0x%04X\n",pOR.p16[0]);
            }
            else cnt=0;
            break;
        case netFREQ:
//            param.p32[0]=BaseFreq;
            if(cnt>=4)
            {
                rtrCopyData((uint16_t)&pOR,buf+pData,4);
                pData+=4;
                cnt-=4;
                printf("FREQ=%lu\n",pOR.p32[0]);
            }
            else cnt=0;
            break;
        case netPOWER:
//            param.p8[0]=0x4c;
            rtrCopyData((uint16_t)&pOR,buf+pData,1);
            ++pData;
            --cnt;
        case netTICK:
//            param.p8[0]=TimeCnt;
            rtrCopyData((uint16_t)&pOR,buf+pData,1);
            ++pData;
            --cnt;
            printf("TICK=0x%02X\n",pOR.p8[0]);
            break;
// Nothing
        default:
            cnt=0;
            if(pTotal) --pTotal;
            break;
        }
    }
    return pTotal;
}
//
uint8_t rtrParse(__xdata uint8_t *RXbuf,uint8_t cnt,__xdata uint8_t *TXbuf)
{
    struct rtrPacket *rP;
    uint8_t  pCnt,pTotal,pData;
    rP=(struct rtrPacket *)RXbuf;

    if(rP->rtrCmd&cmdSEND) pTotal=rtrSaveParam(RXbuf,cnt);
    else
    {
        pData = offsetof(struct rtrPacket,rtrData);
        pCnt= rP->rtrDataLength;
        pTotal=0;
        cnt-=L_BLOCK0;
        while(cnt-- && pCnt--) paramList[pTotal]=rP->rtrData[pTotal++];
        paramList[pTotal]=0xFF;
        pTotal=rtrCreate(TXbuf,rP->rtrCmd|cmdSEND,paramList);
    }
    return pTotal;
}
//
uint8_t rtrCopyData(__xdata uint8_t *Dst,__xdata uint8_t *Src,uint8_t cnt)
{
    while(cnt--) *Dst++ = *Src++;
    return 0;
}
//
uint8_t rtrAddParam(__xdata uint8_t *pData,uint8_t pAdr,uint8_t cmd)
{
    //    union __param
//    {
//        uint32_t p32[1];
//        uint16_t p16[2];
//        uint8_t  p8[4];
//    }  __xdata param;
    *pData++ = pAdr;
    if((cmd&cmdSEND) == 0) return 1;
    else
    {
        switch(pAdr)
        {
        case netADR:
            pOR.p8[0]=devAddress;
            rtrCopyData(pData,(uint16_t)&pOR,1);
            return 1+1;
            break;
        case netSYNC:
            pOR.p16[0]=SyncWord;
            rtrCopyData(pData,(uint16_t)&pOR,2);
            return 1+2;
            break;
        case netFREQ:
            pOR.p32[0]=BaseFreq;
            rtrCopyData(pData,(uint16_t)&pOR,4);
            return 1+4;
            break;
        case netPOWER:
            pOR.p8[0]=0x4c;
            rtrCopyData(pData,(uint16_t)&pOR,1);
            return 1+1;
        case netTICK:
            pOR.p8[0]=TicksMain;
            rtrCopyData(pData,(uint16_t)&pOR,1);
            return 1+1;
            break;
// Nothing
        default:
            *--pData = 0;
            return 0;
            break;
        }
    }
}
//
uint8_t rtrPrintPacket(__xdata uint8_t *pData,uint8_t pCnt)
{
    //    union __param
//    {
//        uint32_t p32[1];
//        uint16_t p16[2];
//        uint8_t  p8[4];
//    }  __xdata param;
    printf(" Adr %u",*pData++);
    printf(" Time %u",*pData++);

    if((*pData++ & cmdSEND) == 0)
    {
        printf("\n");
        return 0;
    }
    pData++;
    pCnt-=4;
//    else
    while((int8_t)(pCnt--) > 0)
    {
        switch(*pData++)
        {
        case netADR:
            printf(" netAdr=%u",*pData++);
            pCnt--;
            break;
        case netSYNC:
            printf(" SYNC=%04X%",*(uint16_t*)pData++);
            pData++;
            pCnt--;
            pCnt--;
            break;
        case netFREQ:
            printf(" FREQ=%lu",*(uint32_t*)pData++);
            pData++;
            pData++;
            pData++;
            pCnt--;
            pCnt--;
            break;
        case netPOWER:
            printf(" PWR=%u",*pData++);
            pCnt--;
            break;
        case netTICK:
            printf(" TIME=%u",*pData++);
            pCnt--;
            break;
        case netLQI:
            printf(" LQI=%u",*pData++);
            pCnt--;
            break;
        case netRSSI:
            printf(" RSSI=%d",*pData++);
            pCnt--;
            break;
// Nothing
        default:
            *--pData = 0;
            pCnt=0;
            break;
        }
    }
    printf("\n");
    return 1;
}
//
uint8_t rtrCreate(__xdata uint8_t *buf,uint8_t cmd,__xdata uint8_t *pList)
{
    uint8_t i,pCnt;
    struct rtrPacket *rP;
    rP=(struct rtrPacket *)buf;
//    wP->rtrGrpAdr=OutPWR;
    rP->rtrAdr=devAddress;
    rP->rtrStartTime = TicksMain;
    rP->rtrCmd=cmd;
//    rP->rtrDataLength=0;
    for(i=offsetof(struct rtrPacket,rtrDataLength); i<MAXPACKET; i++) buf[i]=0;
    pCnt=offsetof(struct rtrPacket,rtrData);
    while(*pList != 0xFF)
    {
//            rP->rtrData[pCnt++]=*pList++;
        pCnt+=rtrAddParam(buf+pCnt,*pList++,cmd);
    }
    rP->rtrDataLength=pCnt;
//    printf("Create %u ",pCnt);
//    HexPrt(buf,pCnt);
//    pCnt=rtrPack(buf,pCnt);
//    printf("Pack %u ",pCnt);
//    HexPrt(buf,pCnt);
    return rtrPack(buf,pCnt);
    //pCnt;
}
//
uint8_t rtrPack(__xdata uint8_t *buf,uint8_t cnt)
{
    uint8_t packed,crc8,pcnt,j;
//cnt = uint8_t rtrEncrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key)
// shift buf to end of memory
    pcnt=PACKETMAX;
    for(packed=cnt; packed>L_BLOCK0;) buf[--pcnt]=buf[--packed];
// Insert CRC8 to Data packet Blocks0
    buf[L_BLOCK0]=rtrCRC8(buf,L_BLOCK0);
    packed=L_BLOCK0+1;
    cnt-=L_BLOCK0;
    while(cnt)
    {
        j=MIN(L_BLOCK1,cnt);
        cnt-=j;
        crc8=rtrCRC8(buf+pcnt,j);
        while(j--) buf[packed++]=buf[pcnt++];
        buf[packed++]=crc8;
    }
    return packed;
}
//
uint8_t rtrUnPack(__xdata uint8_t *buf,uint8_t cnt)
{
// Remove CRC8 from Data packet Blocks
    uint8_t unpacked,packed,j;
    cnt=rtrCheck(buf,cnt);
    if(cnt==0) return 0;
    unpacked=packed=L_BLOCK0;
    packed++; //  remove CRC8 BLOCK0
    cnt-=L_BLOCK0+1;
    while(cnt>1)
    {
        if(cnt>L_BLOCK1) j=L_BLOCK1+1;
        else j=cnt;
        cnt-=j;
        while(--j) buf[unpacked++]=buf[packed++];
        packed++; //  remove CRC8 BLOCK1
    }
    return unpacked;
//return rtrDecrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key)
}
////
//uint32_t rtrComputeFREQ(uint32_t FCarrier)
//{
//#ifndef FOSC
//#define FOSC 26000000
//#endif
//    union b32
//    {
//        uint32_t FREQ;
//        uint8_t fr[4];
//    } __xdata uF= {0};
//    uint32_t f = FOSC;
////        FREQ2=FCarrier/f;
//    uF.fr[2]=FCarrier/f;
////        FCarrier -= f*FREQ2;
//    FCarrier -= f*uF.fr[2];
//    FCarrier <<= 8;
////        FREQ1 = FCarrier/f;
//    uF.fr[1]=FCarrier/f;
////        FCarrier -= f*FREQ1;
//    FCarrier -= f*uF.fr[1];
//    f <<= 8;
////        FREQ0 = FCarrier/f;
//    uF.fr[0]=FCarrier/f;
////    printf(" %02X %02X %02X\n\r",FREQ2,FREQ1,FREQ0);
//    return uF.FREQ;
//}
//
/*  CRC8 Single Error Table Polynom = 0x2F */
const uint8_t tableECC[ECCMAX*8]=
{
    0x2F, 0x5E, 0xBC, 0x57, 0xAE, 0x73, 0xE6, 0xE3,
    0xE9, 0xFD, 0xD5, 0x85, 0x25, 0x4A, 0x94, 0x07,
    0x0E, 0x1C, 0x38, 0x70, 0xE0, 0xEF, 0xF1, 0xCD,
    0xB5, 0x45, 0x8A, 0x3B, 0x76, 0xEC, 0xF7, 0xC1,
    0xAD, 0x75, 0xEA, 0xFB, 0xD9, 0x9D, 0x15, 0x2A,
    0x54, 0xA8, 0x7F, 0xFE, 0xD3, 0x89, 0x3D, 0x7A,
    0xF4, 0xC7, 0xA1, 0x6D, 0xDA, 0x9B, 0x19, 0x32,
    0x64, 0xC8, 0xBF, 0x51, 0xA2, 0x6B, 0xD6, 0x83,
    0x29, 0x52, 0xA4, 0x67, 0xCE, 0xB3, 0x49, 0x92,
    0x0B, 0x16, 0x2C, 0x58, 0xB0, 0x4F, 0x9E, 0x13,
    0x26, 0x4C, 0x98, 0x1F, 0x3E, 0x7C, 0xF8, 0xDF,
    0x91, 0x0D, 0x1A, 0x34, 0x68, 0xD0, 0x8F, 0x31,
    0x62, 0xC4, 0xA7, 0x61, 0xC2, 0xAB, 0x79, 0xF2,
    0xCB, 0xB9, 0x5D, 0xBA, 0x5B, 0xB6, 0x43, 0x86,
    0x23, 0x46, 0x8C, 0x37, 0x6E, 0xDC, 0x97, 0x01,
};
//
#define ECCERRORMAX 1
uint8_t rtrCheck(__xdata uint8_t *buf,uint8_t cnt)
{
    uint8_t checked,j;
    if(cnt < (L_BLOCK0+1)) return  0;       // RADIO
    if(cnt == 255) return  0;               // RADIO
    if(cnt > PACKETMAX) return  0;       // RADIO
// Block 0
    if(rtrECC(buf,(L_BLOCK0+1)) > ECCERRORMAX)
    {
//        printf("CRC error\n");
        return  0;     // RADIO
    }
//  Block 1...N
    checked = L_BLOCK0+1;
    j=buf[offsetof(struct rtrPacket,rtrDataLength)];
    cnt=MIN(cnt,PACKED_CNT(j));
    cnt-=L_BLOCK0+1;
    buf+=L_BLOCK0+1;
    while(cnt>1)   // 1byte + CRC8
    {
        if(cnt > (L_BLOCK1+1)) j=(L_BLOCK1+1);
        else j = cnt;
        if(rtrECC(buf,j) > ECCERRORMAX)
        {
            return checked; // RADIO
        }
        checked += j;
        buf+=j;
        cnt-=j;
    };
    return checked;
}
//
uint8_t rtrECC(__xdata uint8_t *buf,uint8_t cnt)
{
    uint8_t pos,crc8;
    uint8_t i;
    crc8=rtrCRC8(buf,cnt);
// No Errors
    if(!crc8) return 0;
// One Error
    for(i=0; i<cnt*8; i++)
    {
        if(crc8==tableECC[i])
        {
            pos=cnt-1-i/8;
            buf[pos] ^= (1<<(i%8));
            return 1;
        }
    }
// Two Errors
    return 2;
}
//
uint8_t rtrCRC8(__xdata uint8_t *buf,uint8_t cnt)
{
    /*
      Name  : CRC-8
      Poly  : 0x31    x^8 + x^5 + x^4 + 1
      Init  : 0xFF
      Revert: false
      XorOut: 0x00
      Check : 0xF7 ("123456789")
      MaxLen: 15 байт(127 бит) - обнаружение
      одинарных, двойных, тройных и всех нечетных ошибок
    */
    uint8_t i;
    uint8_t crc8 = 0;
    while(cnt--)
    {
        crc8 ^= *buf++;
        for (i = 0; i < 8; i++)
            crc8 = (crc8 & 0x80)?(crc8<<1)^Poly8:(crc8<<1);
    };
    return crc8;
}
////
//uint8_t rtrEncrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key)
//{
//    return cnt;
//}
////
//uint8_t rtrDecrypt(__xdata uint8_t *buf,uint8_t cnt,__xdata uint8_t *Key)
//{
//    return cnt;
//}
//
