//
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "cc1111.h"
#define nop __asm nop __endasm

#define HI(byte) (byte>>8)
#define LO(byte) (byte&0x00FF)
#define BIT(bit)            (1<<bit)
#define sBit(byte,bit)      (byte|=BIT(bit))
#define cBit(byte,bit)      (byte&=~BIT(bit))
#define tBit(byte,bit)      (byte&BIT(bit))
#define fBit(byte,bit)      (byte^=BIT(bit))

#define FromBCD(s)          (10*(s>>4)+(s&0x0F))
#define ToBCD(s)            (((s/10)<<4)|(s%10))

//
#define MIN(A,B) (A<B)?A:B
#define MAX(A,B) (A>B)?A:B

//
//uint8_t StringCopy(uint8_t *buf,char *str)
//{
//    uint8_t cnt=0;
//    while(*str != 0) buf[cnt++]=*str++;
//    buf[cnt++]=0;
//    return cnt;
//}
////

//////
//uint16_t freqCalib(void)
//{
//    uint16_t c1,c2;
//    uint8_t w;
//    T1CTL=0;
//    T1CCTL0=0;
//    T1CNTL=0;
//    w=WORTIME0;
//    while(w==WORTIME0);
//    T1CTL=T1CTL_MODE_FREE;
//    c1=(WORTIME1<<8)|WORTIME0;
//    w=8;
//    while(w--)
//    {
//        while (!(T1CTL&T1CTL_OVFIF));
//        T1CTL&=~T1CTL_OVFIF;
//    }
//    c2=(WORTIME1<<8)|WORTIME0;
//    return (c2-c1);
//}
