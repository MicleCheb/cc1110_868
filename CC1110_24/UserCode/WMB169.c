/**
 * NameofFile.c      CC1110 + CC1120 169 MHz EM
 * Compiler:         Code::Blocks + SDCC for 8051
 * Target:           TI CC1110 SoC
 * Author:           Micle Chebotarev (mcheb@kvadrat-omsk.ru)
 * Data:             16-Dec-2013 Mon
**/
#include "miscUtil.h"
#include "cc1110EB868.h"
#include "cc1120Radio.h"
#include "OpenRadio.h"
//
void main(void)
{
    uint8_t j;
// MCU Initializing
    InitBoard();
// RF initializing
    init169();
    Init868();
//
    EA=1;
    G_LedMask=0;
    R_LedMask=MaskNorma;
    RadioTimer=BoardFreq*6;
    XT2ON;
    printf("\r\nTI 169MHz board\n");
    XT2OFF;
MainLoop:
    while(1)
    {
        if(tBit(Flags,fl868))
        {
            cBit(Flags,fl868);
            G_LedOn;
//            rxCnt=rfRcv868(rx868,MAXPACKET-2);
//            printf("%u\n",rxCnt);
            paramList[0]=netSYNC;
            paramList[1]=netFREQ;
            paramList[2]=netTICK;
            paramList[3]=0xff;
            txCnt=rtrCreate(tx868,cmdSEND|cmdDEVICE,paramList); //0&~cmdSEND
//
            printf("Send868 %u ",txCnt);
            HexPrt(tx868,txCnt);
//
            rfSend868(tx868,txCnt);
            printf("Receive868...");
            RadioTimer=2;
            rxCnt=rfRcv868(rx868,PACKETMAX);
            rxCnt=rtrCheck(rx868,rxCnt);
            rxCnt=rtrUnPack(rx868,rxCnt);
//            rxCnt=rtrCheck(rx868,MAXPACKET);
            if(rxCnt)
            {
                printf("Rcv  %u ",rxCnt);
                HexPrt(rx868,rxCnt);
                BeaconTimer=RadioPause*BoardFreq-1;
//                rtrParse(rx868,rxCnt);
            }
            else printf("No RF868\n");

//
//            rxCnt=rtrUnPack(rx868,MAXPACKET);
//            printf("Rcv  %u ",rxCnt);
//            HexPrt(rx868,rxCnt);
//           rxCnt= rtrParse(rx868,rxCnt,tx868);
//
//            printf("Parse  %u ",rxCnt);
//            HexPrt(tx868,rxCnt);
//
//
//            txCnt=rtrUnPack(tx868,MAXPACKET);
//                        printf("UnPack %u ",txCnt);
//            HexPrt(tx868,txCnt);
//
//           rxCnt= rtrParse(tx868,txCnt,rx868);
//
//            printf("Parse  %u ",rxCnt);
//            HexPrt(rx868,txCnt);
//
//                        printf("DONE\r\n");
            G_LedOff;
//            G_LedMask=0xFF;
//            LedTimer=BoardFreq*2;
//            RadioTimer=2;
//            rfSend(tx868,MAXPACKET);

//        }
//        if(tBit(Flags,fl169))
//        {
//            cBit(Flags,fl169);
//            for(spiCnt=0; spiCnt<64; spiCnt++) tx169[spiCnt]=0xAA;
//            spiWriteMaster(tx169,64);
//                        for(spiCnt=0; spiCnt<64; spiCnt++) tx169[spiCnt]=spiCnt;
//            uartWrite(tx169,64);
//            for(RadioTimer=100; RadioTimer; RadioTimer--)
//            {
//                putchar(0x55);
//                delay(5);
//            }

//            c1=freqCalib();
////            rfSend(tx868,MAXPACKET);
//            XT2ON;
//            //    spiCnt=setUartBaud(); //66;//34;    //70;//131; // 115200 13 MHz clock
//            printf(" P0_7 = %u P2_0 = %u\n",CC1120x_IOCFG3,CC1120x_RESET);
//
//            printf("Reg %u %u\n",c1,c2);
//            RadioTimer=BoardFreq*2;
//            c2=freqCalib();
//            XT2OFF;
//            bNew=(((uint32_t)(256+131))*c1)/c2-256;
//            U0BAUD=bNew;
        }
        if(tBit(Flags,fl169))
        {
            cBit(Flags,fl169);
            printf("   Current RF169 settings\n");
            for(j = 0; j < (sizeof(regSet)/sizeof(struct regSet_t)); j++)
            {
                printf("REG %04X : %02X\n",regSet[j].reg,regSet[j].val);

            }
//            RadioTimer=BoardFreq*2;
//            printf("\rSending... \n");
//
//            CC1120x_TX((uint16_t)&regSet,sizeof(regSet));
//
////            int8_t CC1120x_RX(__xdata uint8_t *rxBuffer,uint8_t rxBytes)
//            printf("\rOK\n");
//            RadioTimer=BoardFreq*2;
//            printf("\rReceive... \n");
//            j=CC1120x_RX((uint16_t)0xF400,64);
////            int8_t CC1120x_RX(__xdata uint8_t *rxBuffer,uint8_t rxBytes)
//            printf("\r %u  OK\n",j);

        }
//
//        R_LedFlp;
        goToSleep();
    };
}
//
//void radio169Tx(void)
//{
//
//    // Calibrate radio according to errata
//    manualCalibration();
//
//    // Infinite loop
//    while(TRUE)
//    {
//
//        // Wait for button push
//        if(bspKeyPushed(BSP_KEY_ALL))
//        {
//
//            // Continiously sent packets until button is pressed
//            do
//            {
//
//                // Update packet counter
//                packetCounter++;
//
//                // Create a random packet with PKTLEN + 2 byte packet
//                // counter + n x random bytes
//                createPacket(txBuffer);
//
//                // Write packet to TX FIFO
//                cc112xSpiWriteTxFifo(txBuffer, sizeof(txBuffer));
//
//                // Strobe TX to send packet
//                trxSpiCmdStrobe(CC1120_STX);
//
//                // Wait for interrupt that packet has been sent.
//                // (Assumes the GPIO connected to the radioRxTxISR function is
//                // set to GPIOx_CFG = 0x06)
//                while(packetSemaphore != ISR_ACTION_REQUIRED);
//
//                // Clear semaphore flag
//                packetSemaphore = ISR_IDLE;
//
//                // Update LCD
//                updateLcd();
//            }
//            while (!bspKeyPushed(BSP_KEY_ALL));
//        }
//    }
//    //
//
//    // Calibrate radio according to errata
//    manualCalibration();
//
//    // Set radio in RX
//    trxSpiCmdStrobe(CC1120_SRX);
//
//    // Infinite loop
//    while(TRUE)
//    {
//
//        // Wait for packet received interrupt
//        if(packetSemaphore == ISR_ACTION_REQUIRED)
//        {
//
//            // Read number of bytes in RX FIFO
//            cc112xSpiReadReg(CC1120_NUM_RXBYTES, &rxBytes, 1);
//
//            // Check that we have bytes in FIFO
//            if(rxBytes != 0)
//            {
//
//                // Read MARCSTATE to check for RX FIFO error
//                cc112xSpiReadReg(CC1120_MARCSTATE, &marcState, 1);
//
//                // Mask out MARCSTATE bits and check if we have a RX FIFO error
//                if((marcState & 0x1F) == RX_FIFO_ERROR)
//                {
//
//                    // Flush RX FIFO
//                    trxSpiCmdStrobe(CC1120_SFRX);
//                }
//                else
//                {
//
//                    // Read n bytes from RX FIFO
//                    cc112xSpiReadRxFifo(rxBuffer, rxBytes);
//
//                    // Check CRC ok (CRC_OK: bit7 in second status byte)
//                    // This assumes status bytes are appended in RX_FIFO
//                    // (PKT_CFG1.APPEND_STATUS = 1)
//                    // If CRC is disabled the CRC_OK field will read 1
//                    if(rxBuffer[rxBytes - 1] & 0x80)
//                    {
//
//                        // Update packet counter
//                        packetCounter++;
//                    }
//                }
//            }
//
//            // Update LCD
//            updateLcd();
//
//            // Reset packet semaphore
//            packetSemaphore = ISR_IDLE;
//
//            // Set radio back in RX
//            trxSpiCmdStrobe(CC1120_SRX);
//        }
//    }
//
//}
//





